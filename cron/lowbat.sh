#!/bin/bash

export DISPLAY=:0
export XAUTHORITY="${HOME}/.Xauthority"

battery_level=`acpi -b | grep -P -o '[0-9]+(?=%)'`
if [ $battery_level -le 25 ]
then
    notify-send "Battery low" "Battery level is ${battery_level}%!"
fi
