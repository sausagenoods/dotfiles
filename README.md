## GPS
![gps](https://gitlab.com/sausagenoods/dotfiles/raw/master/gps/screenshot.png)

## Space
![space](https://gitlab.com/sausagenoods/dotfiles/raw/master/space/screenshot.png)

## Vine
![vine](https://gitlab.com/sausagenoods/dotfiles/raw/master/vine/screenshot.png)

## Magi
![magi](https://gitlab.com/sausagenoods/dotfiles/raw/master/magi/magi.png)
